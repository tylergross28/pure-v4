import Vue from 'vue'
import Vuex from 'vuex'
import db from '../firebase/firebaseInit'

Vue.use(Vuex)

/* eslint-disable no-new */
export const store = new Vuex.Store({
  state: {
    categories: []
  },
  mutations: {
    computeCategories: state => {
      let catRef = db.collection('product-categories')
      catRef.where('active', '==', true)
        .orderBy('index')
        .get()
        .then(function (querySnapshot) {
          querySnapshot.forEach(function (doc) {
            var catData = doc.data()
            // console.log(catData)
            state.categories.push(catData)
          })
        })
        .catch(function (error) {
          console.log('Error getting documents: ', error)
        })
    }
  },
  actions: {
    computeCategories: context => {
      context.commit('computeCategories')
    }
  }
})
